NP=1

default:
	@export PYTHONPATH=${PWD}; \
	mpirun -n ${NP} ilamb-run --config co2.cfg --model_setup clm.txt

clean:
	rm -rf _build *~ *.pyc
