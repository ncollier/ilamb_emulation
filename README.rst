CO2 Emulation in ILAMB
======================

This repository is a plugin to the `ILAMB <https://bitbucket.org/ncollier/ilamb>`_ benchmarking system. In order to make use of this code, you will need to have ILAMB installed. For details, follow the installation instructions found `here <https://ilamb.ornl.gov/doc/tutorial.html>`_. Once ILAMB is installed, then you will need to:

* clone this repository:  ``git clone https://ncollier@bitbucket.org/ncollier/ilamb_emulation.git``
* enter into the cloned directory:  ``cd ilamb_emulation``
* set your environment variable to point to where you want the observational data and pulse files to be downloaded:  ``export ILAMB_ROOT=FULL_PATH_TO_YOUR_DIRECTORY``
* download the observational data and pulse files by running the command:  ``ilamb-fetch --remote_root http://ilamb.ornl.gov/CO2-Data``
* set your python path to also look in the current directory for files to import:  ``export PYTHONPATH=${PWD}``

Now you are ready to run ILAMB against your own selection of model outputs. For sample output against 3 versions of CLM, see this `site <https://www.climatemodeling.org/~nate/CO2/EcosystemandCarbonCycle/CarbonDioxide/NOAA/NOAA.html>`_.
